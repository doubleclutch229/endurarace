<div class="p-6 sm:px-20 bg-white border-b border-gray-200">
    <div>
        <x-jet-application-logo class="block h-12 w-auto" />
    </div>

    <div class="mt-2 text-xl">
        Welcome to your Endura Race Dashboard!
    </div>

    <div class="mt-2 text-gray-500">
       EnduraRace LLC is a company committed to bringing the best in ultra marathon and adventure events to cyclists and runners alike.
    </div>
</div>


    <div class="bg-white">
        <div class="p-6">

            <div class="ml-6 text-lg text-gray-600 leading-7 font-semibold">Sponsers</div>

                    <div  class="ml-6" style="display: table;">
                        <div style="display: table-row">
                            <div style="display: table-cell;">  @include('wolftooth-logo') </div>
                            <div style="display: table-cell;">  @include('orangeseal-logo') </div>
                        </div>
                    </div>

            </div>

        <div class="p-6 border-t border-gray-200">
            <div class="flex items-center">
                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-400"><path d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
                <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold">Forum</div>
            </div>

            <div class="ml-12">
                <div class="mt-2 text-sm text-gray-500">
                    Laravel Jetstream is built with Tailwind, an amazing utility first CSS framework that doesn't get in your way. You'll be amazed how easily you can build and maintain fresh, modern designs with this wonderful framework at your fingertips.
                </div>
            </div>
        </div>

        <div class="p-6 border-t border-gray-200 md:border-l">
            <div class="flex items-center">
                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-400"><path d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path></svg>
                <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold">Blog</div>
            </div>

            <div class="ml-12">
                <div class="mt-2 text-sm text-gray-500">
                    Authentication and registration views are included with Laravel Jetstream, as well as support for user email verification and resetting forgotten passwords. So, you're free to get started what matters most: building your application.
                </div>
            </div>
        </div>
    </div>
</div>
